import React, { Component } from "react";
import "./App.css";

import NewGame from './components/NewGame'
import Game from './components/Game'

import localforage from 'localforage'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      today: 0,
      games: null
    }
  }
  componentDidMount(){
    localforage.keys().then((keys) => {
      // An array of all the key names.
      if(keys.length > 0){
        this.setState({games: keys});
      }
    }).catch(function(err) {
      // This code runs if there were any errors
      console.log(err);
    });
  }
  render() {
    if(this.state.games != null){
      return <div><Game save={this.state.games[0]}/></div>
    }
    else{
      return (
      <div>
        <NewGame/>
      </div>)
    }
  }
}

export default App;
