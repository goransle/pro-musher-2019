import React, { Component } from "react";
import PropTypes from "prop-types";
import localforage from "localforage";

import { RenderDogs } from "./Dog";

import Race, { RenderRace } from "./Race";

const Finnmarksløpet = new Race(
  "Finnmarksløpet",
  8,
  [
    { name: "Alta", distance: 0 },
    { name: "Keutokeino", distance: 130 },
    { name: "Jergul", distance: 100 },
    { name: "Levajok", distance: 120 },
    { name: "Tana bru", distance: 100 },
    { name: "Neiden 1", distance: 95 },
    { name: "Øvre Pasvik", distance: 75 },
    { name: "Kirkenes", distance: 85 },
    { name: "Neiden 2", distance: 75 },
    { name: "Varangerbotn", distance: 80 },
    { name: "Levajok 2", distance: 125 },
    { name: "Karasjok", distance: 85 },
    { name: "Jotka", distance: 85 },
    { name: "Alta", distance: 50 }
  ],
  {rest: {total: 26, stops: [16]}}
);

export default class Game extends Component {
  static propTypes = {
    save: PropTypes.string
  };
  constructor(props) {
    super(props);
    this.state = {
      gameIsLoaded: false,
      dogs: [],
      races: [Finnmarksløpet],
      activeView: "Home",
      time: 0
    };
  }
  componentDidMount() {
    this.getState();
  }
  getState = () => {
    localforage
      .getItem(this.props.save)
      .then(value => {
        this.setState({
          gameIsLoaded: true,
          dogs: value.dogs,
          activeView: value.activeView
        });
        console.log(value);
      })
      .catch(function(err) {
        // This code runs if there were any errors
        console.log(err);
      });
  };
  saveState = () => {
    localforage.setItem(this.props.save, this.state).then(() => {
      console.log("Game saved!");
    });
  };
  setView = e => {
    this.setState({ activeView: e.target.innerHTML });
  };
  componentDidUpdate() {
    this.saveState();
  }
  render() {
    if (this.state.gameIsLoaded) {
      return (
        <React.Fragment>
          <div id="menu">
            <h1>Menu</h1>
            <button onClick={this.setView}>Home</button>
            <button onClick={this.setView}>Kennel</button>
            <button onClick={this.setView}>Race</button>
          </div>
          <div id="main">
            {this.state.activeView === "Kennel" && (
              <React.Fragment>
                <RenderDogs dogs={this.state.dogs} />
                <button onClick={this.saveState}>Save Changes</button>
              </React.Fragment>
            )}
            {this.state.activeView === "Home" && <h1>Hello world</h1>}
            {this.state.activeView === "Race" && (
              <RenderRace
                race={Finnmarksløpet}
                time={0}
                dogs={this.state.dogs}
              />
            )}
          </div>
        </React.Fragment>
      );
    } else {
      return <div />;
    }
  }
}
