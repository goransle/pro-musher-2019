import React, { Component } from 'react'
import localForage from 'localforage'

import Dog from './Dog'

var names = ["Russel", "De Niro", "Max", "Alex", "Balder", "Balto", "Fant", "Robert"]


export default class NewGame extends Component {
  constructor(props){
    super(props);
    this.state = {saveName:"", dogs: this.generateDogs(names)}
  }
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onSubmit = form => {
    var game = {dogs: this.state.dogs};
    localForage.setItem(this.state.saveName, game);
  };
  generateDogs = (names) =>{
    var dogs = [];
    names.forEach(name => {
      dogs.push(new Dog(name,6,1,6))
    });
    return dogs;
  }

  render() {
    return (
      <div>
        <p>You set out to become the greatest dog musher in the world!</p>
        <form onSubmit={this.onSubmit}>
          <label>What is your name?</label>
          <input name="saveName" type="text" onChange={this.handleChange} defaultValue="Mr. Musher"/>
          <br/>
          <input type="submit" value="Start game" />
        </form>
      </div>
    )
  }
}