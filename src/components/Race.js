import React, { Component } from "react";

export default class Race {
  constructor(name = "", duration = Number(), checkpoints = [], rules = {}) {
    this.name = name;
    this.duration = duration;
    this.checkpoints = checkpoints;
    this.length = sum(this.checkpoints);
  }
}

function sum(checkpoints) {
  var total = 0;
  checkpoints.forEach(checkpoint => {
    total = total + checkpoint.distance;
  });
  return total;
}

export class RenderRace extends Component {
  state = {
    raceStarted: false,
    time: 0,
    start: Date.now(),
    distance: 0,
    distanceToCheckpoint: 0,
    passed: ["Alta"],
    speed: 0,
    dogs: this.props.dogs,
    atCheckPoint: true,
    rest: 0,
    hoursToRest: 5
  };
  startRace = () => {
    this.setState({
      raceStarted: true,
      start: Date.now() - this.state.time,
      atCheckPoint: false,
      speed: (this.props.dogs[0].stats.speed / 10) * 10 * 25
    });
  };
  stopRace = () => {
    this.setState({ raceStarted: false });
  };
  atCheckPoint = () => {
    this.stopRace();
    this.setState({ atCheckPoint: true });
  };
  rest = () =>{
      this.setState({rest: this.state.rest + this.state.hoursToRest})
  }
  onChange =(e) =>{
      var value = Number(e.target.value)
      if(e.target.value)
        this.setState({hoursToRest: value})
  }
  componentDidMount() {
    var sum = 0;
    var distances = this.props.race.checkpoints.map(checkpoint => {
      return (sum += checkpoint.distance);
    });

    this.timer = setInterval(() => {
      if (this.state.raceStarted) {
        this.setState({
          time: Date.now() - this.state.start,
          distance: this.state.speed * (Date.now() - this.state.start)/1000,
          distanceToCheckpoint: Number(
            distances[this.state.passed.length] - this.state.distance
          ).toFixed()
        });
        //sjekke om checkpoints er passerte
        if (
          this.state.distance >= distances[this.state.passed.length] &&
          this.state.passed.length < this.props.race.checkpoints.length - 1
        ) {
          this.setState({
            passed: [
              ...this.state.passed,
              this.props.race.checkpoints[this.state.passed.length].name
            ]
          });
          this.atCheckPoint();
        }
      }
    }, 1);
  }
  render() {
    var checkPointUI;
    if (this.state.atCheckPoint) {
      checkPointUI = (
        <div>
          At checkpoint {this.state.passed[this.state.passed.length - 1]}
          <br />
          <input type="number" onChange={this.onChange}/>
          <button onClick={this.rest}>Rest for {this.state.hoursToRest} hours</button>
        </div>
      );
    } else {
      checkPointUI = `Next checkpoint: ${
        this.props.race.checkpoints[this.state.passed.length].name
      } in ${this.state.distanceToCheckpoint} km`;
    }
    return (
      <div>
        {this.props.race.name}
        <br />
        {Number(this.state.time / 1000).toFixed()} hours sledding
        <br />
        {this.state.rest} hours resting
        <br/>
        {Number(this.state.distance).toFixed()} km /
        {this.props.race.length + "km"}
        <br />
        {this.state.speed} km/h
        <br />
        {this.props.race.checkpoints.length + " Stages"}
        <br />
        Checkpoints passed: {this.state.passed}
        <br />
        {checkPointUI}
        <br />
        <button onClick={this.startRace}>Start race</button>
        <button onClick={this.stopRace}>Stop race</button>
      </div>
    );
  }
}
