import React, { Component } from "react";
import PropTypes from "prop-types";

export default class Dog {
  constructor(name, age, min, max) {
    this.name = name;
    this.age = age;
    this.gender = ["m", "f"][Math.floor(Math.random() * ["m", "f"].length)];
    this.energy = 100;
    this.stats = {
      stamina: getRandomInt(min, max),
      speed: getRandomInt(min, max),
      mental: getRandomInt(min, max),
      leadership: getRandomInt(min, max)
    };
    this.injuries = {};
    this.bonds = {};
    this.retired = false;
  }
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export class RenderDogs extends Component {
  static propTypes = {
    dogs: PropTypes.array
  };
  state = {
    edit: false,
    newName: ""
  };

  rename = e => {
    this.props.dogs[
      e.target.parentElement.getAttribute("dog-id")
    ].name = this.state.newName;
    this.forceUpdate();
  };
  retire = e => {
    this.props.dogs[
      e.target.parentElement.getAttribute("dog-id")
    ].retired = true;
    console.log(e.target);
    this.forceUpdate();
  };
  toggleEdit = (e, prevState) => {
    this.setState({ edit: !prevState });
  };
  newName = e => {
    this.setState({ newName: e.target.value });
  };
  render() {
    let content = this.props.dogs.map((dog, i) => {
      return (
        <div key={i} dog-id={i} className="dog">
          <h1>{dog.name}</h1>
          {this.state.edit && (
            <React.Fragment>
              <input
                type="text"
                name="newName"
                id="newName"
                onChange={this.newName}
                defaultValue={dog.name}
              />
              <button onClick={this.rename}>Rename</button>
            </React.Fragment>
          )}
          <br />
          {dog.age} year old {dog.gender} {dog.retired && "Retired"}
          {!dog.retired && this.state.edit && (
            <button onClick={this.retire}>Retire</button>
          )}
          <p>{dog.energy}</p>
          <table>
            <tbody>
            <tr><th>Stats</th></tr>
            <tr><td>Stamina</td><td>{dog.stats.stamina}</td></tr>
            <tr><td>Speed</td><td>{dog.stats.speed}</td></tr>
            <tr><td>Leadership</td><td>{dog.stats.leadership}</td></tr>
            <tr><td>Mental</td><td>{dog.stats.mental}</td></tr>
            </tbody>
          </table>
        </div>
      );
    });
    return (
      <React.Fragment>
        <button
          onClick={e => {
            this.toggleEdit(e, this.state.edit);
          }}
        >
          {(!this.state.edit && "Edit") || "Cancel"}
        </button>
        <div id="kennel">
          {content}
        </div>
      </React.Fragment>
    );
  }
}
